<?php
include_once('../vendor/autoload.php');
include 'namespace.php';
include('header.php');
include_once ('printscript.php');

$objBalancesheet=new \App\ExpenseIncome\Balancesheet();
$objBalancesheet->setData($_GET);
var_dump($objBalancesheet->openingbalance());
?>
<div align="center" class="content">
	<div class="container ctn">
		<div align="center" class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success ' id='message'> $msg </div> </div>"; ?>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
		<form action="trashmultiple.php" method="post" id="multiple">
			<div class="container">
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<div class="navbar-header">
							<button style="background-color: #8aa6c1;" type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#navbarTwo" aria-expanded="false" aria-controls="navbarTwo">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<?php
						$userButton= "<div id=\"navbarTwo\" class=\"navbar-collapse collapse\">
                        <ul class=\"nav navbar-nav navbar-right\">
                            <button type=\"button\"   id=\"btnPrint\" value=\"Print Div Contents\" class=\"btn btn-primary active \">Print</button>
                            <a href=\"pdf.php\" class=\"btn btn-primary \"  >Download as PDF</a>
                            <a href=\"xl.php\" class=\"btn btn-primary\" \" >Download as XL</a>
                            <a href=\"email.php?list=1\" class=\"btn btn-primary\" role=\"button\" >Email</a>
                            ";
						$adminButton="<button type=\"button\" class=\"btn btn-danger\" id=\"delete\">Delete  Selected</button>
                            <button type=\"submit\" class=\"btn btn-warning\">Trash Selected</button>
                            <a href=\"trashed . php ? Page = 1\"   class=\"btn btn-info\" > View Trashed List</a>
                            </ul></div>
                        ";
						if($singleUser->role=='admin'){
							echo $userButton.$adminButton;
						} else{ 	echo $userButton; 	}

						?>

					</div>
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="container text-center " style="padding: 0 0 5px 0;" >
				<h1> <?php
					/*if(($_REQUEST['customerId'])=='all') {
                    echo "All Orders";} else{ echo $customeName;}
    */
					?> </h1>
			</div>
			<div class="container">
				<div id="dvContainer" align="center">
					<style>
						<?php
                        include ('../resource/css/printsetup.css');
                         include ('../resource/css/bootstrap.min.css');
                        ?>
					</style>
					<table id="" width="780px" >
						<thead>
						<tr>
							<td colspan="3" align="center" >
								<font  style="text-align: center;  text-transform:uppercase; font-weight: bold; font-size:25px;">Bhatiyari Ship Breakers Ltd.</font> <br>
								<font style="font-size:14px">Bhatiyari, Sitakunda, Chittagong.</font><br>
								<font style="font-size:13px">(<?php echo "Balance Balance Since : ".$_GET['fromTransaction']." to ".$_GET['toTransaction'];?>)</font>
							</td>
						</tr>
						<tr><td ><b><?php  echo "Branch :".$branch; ?></b></td> <td></td> <td style="text-align: right; font-size: 12;"><?php echo "Print Date: ";  echo date('Y-m-d'); ?> </td></tr>
						</thead>
						<tr> <td colspan="3">
								<!-- Inner Table -->
								<div class="row" align="center">
									<div id="reporttable" class="col-sm-12 text-center" align="center" >
										<div class="row">

											<div class="col-md-12">
												<form class="multipleTranscation">
													<div class="control">
														<div class="row">
															<div class="col-md-12 text-center">

																<strong>(Trading Account for the Year Ended)</strong>
															</div>

															</div>
														</div>
													</div><hr/>
													<div class="row">
														<div class="col-md-12">
															<h3 align="left">By</h3>
															<table class="table table-stripped">
															<thead>
															<tr>
																<th>Description</th>
																<th></th>
																<th>Amount</th>
															</tr>
															</thead>
														<tr>
															<td class="text-left">Sales(As per Annexture-1)</td>
															<td>:</td>
															<td>2000</td>
														</tr>
														<tr>
															<td class="text-left">Stock at End(As per Annexture-2)</td>
															<td>:</td>
															<td>2000</td>
														</tr>
														<tr>
															<td class="text-left">Cutting</td>
															<td>:</td>
															<td>2000</td>
														</tr>
														<tr>
															<td class="text-left">Wages</td>
															<td>:</td>
															<td>2000</td>
														</tr>
														<tr>
															<td class="text-right"><strong>Total</strong></td>
															<td></td>
															<td><strong>6000</strong></td>
														</tr>

													</table>
														</div>

													</div>
													<div class="row">
														<div class="col-md-12">
															<h3 align="left">To</h3>
															<table class="table table-stripped">
																<thead>
																<tr>
																	<th>Summery</th>
																	<th></th>
																	<th>Balance</th>
																</tr>
																</thead>
																<tr>
																	<td class="text-left">Opening Stock (As Per annexture-3)</td>
																	<td>2000</td>
																	<td></td>
																</tr>

																<tr>
																	<td class="text-left">Purchase Raw Matterials</td>
																	<td>2000</td>
																	<td></td>
																</tr>
																<tr>
																	<td class="text-left">wages</td>
																	<td>2000</td>
																	<td></td>
																</tr>
																<tr>
																	<td class="text-left">Carriage Inward</td>
																	<td>2000</td>
																	<td></td>
																</tr>
																<tr>
																	<td class="text-left">Electricity</td>
																	<td>2000</td>
																	<td></td>
																</tr>
																<tr>
																	<td class="text-left">Gross Profit Transfer to P/L Account</td>
																	<td>2000</td>
																	<td></td>
																</tr>
																<tr>
																	<td class="text-right"><strong>Total</strong></td>
																	<td></td>
																	<td><strong>6000</strong></td>
																</tr>

															</table>
														</div>
													</div><hr/>
<!-- Profit and Loss Account -->
													<div class="row">
														<div class="text-center">
															<strong>Profit Sheet</strong>
															<p>For the year of ended on 03-12-2018</p>
														</div>
														<div class="col-md-12 ">
															<div class="text-left" > <font size="20">By</font></div>
															<table class="table table-stripped">
																<thead>
																<tr>
																	<th>Summery</th>
																	<th></th>
																	<th>Balance</th>
																</tr>
																</thead>
																<tr>
																	<td class="text-left">Opening Balance</td>
																	<td>:</td>
																	<td class="text-left">2000</td>
																</tr>
																<tr>
																	<td>Cutting</td>
																	<td>:</td>
																	<td>2000</td>
																</tr>
																<tr>
																	<td>Wages</td>
																	<td>:</td>
																	<td>2000</td>
																</tr>
																<tr>
																	<td class="text-right"><strong>Total</strong></td>
																	<td></td>
																	<td><strong>6000</strong></td>
																</tr>

															</table>
														</div>
														<div class="col-md-6">
															<h3>By</h3>
															<table class="table table-stripped">
																<thead>
																<tr>
																	<th>Summery</th>
																	<th></th>
																	<th>Balance</th>
																</tr>
																</thead>
																<tr>
																	<td>Opening Balance</td>
																	<td>:</td>
																	<td>2000</td>
																</tr>
																<tr>
																	<td>Cutting</td>
																	<td>:</td>
																	<td>2000</td>
																</tr>
																<tr>
																	<td>Wages</td>
																	<td>:</td>
																	<td>2000</td>
																</tr>
																<tr>
																	<td class="text-right"><strong>Total</strong></td>
																	<td></td>
																	<td><strong>6000</strong></td>
																</tr>

															</table>
														</div>
													</div>
													<hr/>
													<div class="row">
														<div class="text-center">
															<strong>Balance Sheet</strong>
															<p>As on 30-12-2018</p>
														</div>
														<div class="col-md-6">
															<h3>To</h3>
															<table class="table table-stripped">
																<thead>
																<tr>
																	<th>Summery</th>
																	<th></th>
																	<th>Balance</th>
																</tr>
																</thead>
																<tr>
																	<td>Opening Balance</td>
																	<td>:</td>
																	<td>2000</td>
																</tr>
																<tr>
																	<td>Cutting</td>
																	<td>:</td>
																	<td>2000</td>
																</tr>
																<tr>
																	<td>Wages</td>
																	<td>:</td>
																	<td>2000</td>
																</tr>
																<tr>
																	<td class="text-right"><strong>Total</strong></td>
																	<td></td>
																	<td><strong>6000</strong></td>
																</tr>

															</table>
														</div>
														<div class="col-md-6">
															<h3>By</h3>
															<table class="table table-stripped">
																<thead>
																<tr>
																	<th>Summery</th>
																	<th></th>
																	<th>Balance</th>
																</tr>
																</thead>
																<tr>
																	<td>Opening Balance</td>
																	<td>:</td>
																	<td>2000</td>
																</tr>
																<tr>
																	<td>Cutting</td>
																	<td>:</td>
																	<td>2000</td>
																</tr>
																<tr>
																	<td>Wages</td>
																	<td>:</td>
																	<td>2000</td>
																</tr>
																<tr>
																	<td class="text-right"><strong>Total</strong></td>
																	<td></td>
																	<td><strong>6000</strong></td>
																</tr>

															</table>
														</div>
													</div>

												</form>
											</div>

										</div>


										<br>
									</div>

								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</form>

	</div>
</div>

<?php
include ('footer.php');
include ('footer_script.php');
?>
