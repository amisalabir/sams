<?php

include_once('../../../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('../User/Profile/signup.php');
    return;
}

############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {

    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('../User/Profile/signup.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
################################ End of Session time calculation ##############################

$msg = Message::getMessage();

############################## Message code ended #############################################

include ('header.php');
?>

<div class="container">

<?php

if(isset($_POST['mark']) || isset($_SESSION['mark'])) {    // start of boss if
   $someData=null;
   $objBookTitle= new App\BookTitle\BookTitle();


   if(isset($_POST['mark']) ){
    $_SESSION['mark'] = $_POST['mark'];
    $someData =  $objBookTitle->listSelectedData($_SESSION['mark']);
   }

   echo "<h1> Are you sure you want to delete all selected data?</h1>";
   $confirmation="<a href='deletemultiple.php?YesButton=1' class='btn btn-danger'>Yes</a>
          <a href='index.php' class='btn btn-success'>No</a>
        ";


    if(isset($_GET['YesButton'])){
        $objBookTitle->deleteMultiple($_SESSION['mark']);
        unset($_SESSION['mark']);
    }
?>


    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>


            <th style='width: 10%; text-align: center'>Serial Number</th>
            <th style='width: 10%; text-align: center'>ID</th>
            <th>Book Name</th>
            <th>Author Name</th>
        </tr>

        <?php
        $serial = 1;

        foreach ($someData as $oneData) { ########### Traversing $someData is Required for pagination  #############

            if ($serial % 2) $bgColor = "AZURE";
            else $bgColor = "#ffffff";

            echo "

                  <tr  style='background-color: $bgColor'>


                     <td style='width: 10%; text-align: center'>$serial</td>
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->book_name</td>
                     <td>$oneData->author_name</td>


                  </tr>
              ";
            $serial++;
        }
        ?>

    </table>

</div>
<div class="container text-center">
<?php

    echo "<a href='deletemultiple.php?YesButton=1' class='btn btn-danger'>Yes</a>
          <a href='index.php' class='btn btn-success'>No</a>
        ";

}  // end of boos if
else
{
    Message::message("Empty Selection! Please select some records.");
    //Utility::redirect($_SERVER["HTTP_REFERER"]);
    Utility::redirect('index.php');
}


?>


</div>

<?php

include ('footer.php');
include ('footer_script.php');

?>