<?php 
include('header.php');
?>
	<div class="content">
		<div class="container ctn">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6 main">
					<form class="signleTranscation">
						<div class="control">
							<div class="row">
								<div class="col-md-6">
									<a href="#" class="btn btn-secondary">EDIT</a>
									<a href="#" class="btn btn-secondary">Refresh</a>
								</div>
								<div class="col-md-6">
									<p class="nick text-right">Party Information</p>
								</div>
							</div>
						</div>
						<table class="table table-responsive" border="0">
							<tr>
								<td>Party Name</td>
								<td>:</td>
								<td><input type="text" class="form-control" name="partyName" required></td>
							</tr>
							<tr>
								<td>Address</td>
								<td>:</td>
								<td><input type="text" class="form-control" name="address" required></td>
							</tr>
							<tr>
								<td>Contact No.</td>
								<td>:</td>
								<td><input type="number" class="form-control" name="number" required></td>
							</tr>
							<tr>
								<td>Email ID</td>
								<td>:</td>
								<td><input type="email" class="form-control" name="email" required></td>
							</tr>
							<tr>
								<td>Web Address</td>
								<td>:</td>
								<td><input type="text" class="form-control" name="webAddress" required></td>
							</tr>
							<tr>
								<td>A.P. Name</td>
								<td>:</td>
								<td><input type="text" class="form-control" name="APname" required></td>
							</tr>
							<tr>
								<td>A.P. Contact No</td>
								<td>:</td>
								<td><input type="number" class="form-control" name="APnum" required></td>
							</tr>
							<tr>
								<td>Status	</td>
								<td>:</td>
								<td> <select name="status" class="form-control" required>
										  <option value="ACTIVE" selected>ACTIVE</option>
										  <option value="INACTIVE">INACTIVE</option>
										</select></td>
							</tr>
							
							<tr>
								<td></td>
								<td></td>
								<td>
									<div class="col-auto form-inline">
										<input style="margin-right:60px;" type="submit" class="btn btn-primary" name="submit" value="Save">
										<input type="submit" class="btn btn-primary" name="update" value="Update">
									</div>
								</td>
							</tr>
						</table>
					</form>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	</div>
 <?php 
include('footer.php');
?> 
 