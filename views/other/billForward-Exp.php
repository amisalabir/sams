<?php 
include('header.php');
?>
	<div class="content">
		<div class="container ctn">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6 main">
					<form class="signleTranscation">
						<div class="control">
							<div class="row">
								<div class="col-md-6">
									<a href="#" class="btn btn-secondary">EDIT</a>
									<a href="#" class="btn btn-secondary">Refresh</a>
								</div>
								<div class="col-md-6">
									<p class="nick text-right">Bill Forwarding - Export</p>
								</div>
							</div>
						</div>
						<table class="table table-responsive" border="0">
							<tr>
								<td>Office Name</td>
								<td>:</td>
								<td><input type="text" class="form-control" name="OffName" required></td>
							</tr>
							<tr>
								<td>Party Name</td>
								<td>:</td>
								<td><input type="text" class="form-control" name="partyName" required></td>
							</tr>
							<tr>
								<td>Bill Forward Date </td>
								<td>:</td>
								<td><input type="date" class="form-control" name="forwDate" required></td>
							</tr>
							<tr>
								<td>Remarks</td>
								<td>:</td>
								<td><input type="text" class="form-control" name="remarks" required></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td>
									<div class="col-auto form-inline">
										<input type="submit" class="btn btn-primary" name="Search" value="View Report">
									</div>
								</td>
							</tr>
						</table>
					</form>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	</div>
 <?php 
include('footer.php');
?> 
 