<?php 
include('header.php');
?>
	<div class="content">
		<div class="container ctn">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6 main">
					<form class="signleTranscation">
						<div class="control">
							<div class="row">
								<div class="col-md-6">
									<a href="#" class="btn btn-secondary">EDIT</a>
									<a href="#" class="btn btn-secondary">Refresh</a>
								</div>
								<div class="col-md-6">
									<p class="nick text-right">Job Register</p>
								</div>
							</div>
						</div>
						<table class="table table-responsive" border="0">
							<tr>
								<td>Office Name</td>
								<td>:</td>
								<td><input type="text" name="OfficeName" class="form-control" required></td>
							</tr>
							<tr>
								<td>Job Type</td>
								<td>:</td>
								<td> <select name="jobType" class="form-control" required>
										  <option value="">Select Any</option>
										  <option value="Export">Export</option>
										  <option value="Import">Import</option>
										</select></td>
							</tr>
							<tr>
								<td>Job Year</td>
								<td>:</td>
								<td><input type="text" name="jobYear" class="form-control" required></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td>
									<div class="col-auto form-inline">
										<input style="margin-right:60px;" type="submit" class="btn btn-primary" name="search" value="View Report">
									</div>
								</td>
							</tr>
						</table>
					</form>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	</div>
 <?php 
include('footer.php');
?> 
 