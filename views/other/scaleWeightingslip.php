<?php 
include('header.php');
?>
	<div class="content">
		<div class="container ctn">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10 main">
					<form class="multipleTranscation">
						<div class="control">
							<div class="row">
								<div class="col-md-6">
									<a href="#" class="btn btn-secondary">EDIT</a>
									<a href="#" class="btn btn-secondary">Refresh</a>
								</div>
								<div class="col-md-6">
									<p class="nick text-right">Scale Weighting Slip - Input</p>
								</div>
							</div>
						</div><hr/>
						<table class="table-responsive one">
							<tr>
								<!--<td style="padding-right:10px;">
									<div class="col-auto form-inline">
										Voucher Type : 
										 <select class="form-control" required>
										  <option value="">Select Any</option>
										  <option value="Payment">Payment</option>
										  <option value="Receipt">Receipt</option>
										  <option value="Journal">Journal</option>
										  <option value="Contra">Contra</option>
										</select>
									</div>
								</td>-->
								<td style="padding-right:10px;">
									<div class="col-auto form-inline">
										Date & Time:
										 <input type="date" name="date" class="form-control" required>
										 <input type="time" name="time" class="form-control" required>
									</div>
								</td>
								<td style="padding-right:10px;">
									<div class="col-auto form-inline">
										No :
										 <input type="number" name="vNo" class="form-control" required>
									</div>
								</td>
							</tr>
						</table><br/>
						<table class="table table-responsive">
							<tr>
								<td>Truck No.</td>
								<td>:</td>
								<td><input type="text" name="truckNo" class="form-control" required></td>
								
								<td>Gross</td>
								<td>:</td>
								<td><input type="text" name="gross" class="form-control" required></td>
							</tr>
							<tr>
								<td>Goods Name</td>
								<td>:</td>
								<td><input type="text" name="goodsName" class="form-control" required></td>
								
								<td>Tare</td>
								<td>:</td>
								<td><input type="text" name="tare" class="form-control" required></td>
							</tr>
							<tr>
								<td>Goods Quality</td>
								<td>:</td>
								<td><input type="text" name="goodsQuality" class="form-control" required></td>
								
								<td>NET</td>
								<td>:</td>
								<td><input type="text" name="net" class="form-control" required></td>
							</tr>
							
							<tr>
								<td>Ship Name</td>
								<td>:</td>
								<td><input type="text" name="shipName" class="form-control" required></td>
								
								<td>Gross Time</td>
								<td>:</td>
								<td>
									<div class="col-auto form-inline">
										 <input type="date" name="grossDate" class="form-control" required>
										 <input type="time" name="grossTime" class="form-control" required>
									</div>
								</td>
							</tr>
							<tr>
								<td>Party's Name</td>
								<td>:</td>
								<td><input type="text" name="partysName" class="form-control" required></td>
								
								<td>Tare Time</td>
								<td>:</td>
								<td>
									<div class="col-auto form-inline">
										 <input type="date" name="tareDate" class="form-control" required>
										 <input type="time" name="tareTime" class="form-control" required>
									</div>
								</td>
							</tr>
							<tr>
								<td>Scale CHR.</td>
								<td>:</td>
								<td><input type="text" name="SCHR" class="form-control" required></td>
								
								<td>Excavator CHR.</td>
								<td>:</td>
								<td><input type="text" name="ex.CHR" class="form-control" required></td>
							</tr>
							<tr>
								<td>Driver</td>
								<td>:</td>
								<td><input type="text" name="driver" class="form-control" required></td>
								
								<td>Operator</td>
								<td>:</td>
								<td><input type="text" name="operator" class="form-control" required></td>
							</tr>
						</table>
						<div align="center"><input type="submit" class="btn btn-primary" name="save" value="Save"></div>
					</form>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</div>
 <?php 
include('footer.php');
?> 
 