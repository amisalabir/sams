
<div class="content">
    <div class="container ctn">
        <div class="row">  <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success' id='message'> $msg</div> </div>"; ?> </div>
        <div class="container"><br></div>
        <div class="row">
    <script type="text/javascript">

    </script>
              <form class="form-group" name="BankEntry" action="update.php" method="post">
                <input hidden name="updateBank" type="text" value="updateBank">
                <input hidden name="bankId" type="text" value="<?php echo $objBankToArray['0']['id']; ?>">
                <input name="modifiedDate"  type="text" hidden  value="<?php echo date('Y-m-d');?>">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-4 text-right form-group "><label for="bankname">BANK NAME :</label> </div>
                            <div class="col-sm-4 text-left">
                                <input class="form-control " id="" name="bankname" value="<?php echo $objBankToArray['0']['bankname']; ?>" required type="text">
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 text-right form-group "><label for="accountname">ACCOUNT NAME:</label> </div>
                            <div class="col-sm-4 text-left ">
                                <input class="form-control " id="" name="accountname" value="<?php echo $objBankToArray['0']['accountname']; ?>"required type="text">
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 text-right form-group "><label for="accountnumber">ACCOUNT NUMBER :</label> </div>
                            <div class="col-sm-4 text-left ">
                                <input class="form-control " id="" name="accountnumber" value="<?php echo $objBankToArray['0']['accountnumber']; ?>" required type="text">
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 text-right form-group "><label for="branch">BRANCH :</label> </div>
                            <div class="col-sm-4 text-left ">
                                <input class="form-control " id="" name="branch" value="<?php echo $objBankToArray['0']['branch']; ?>" required type="text">
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 text-right form-group "><label for="address">ADDRESS :</label> </div>
                            <div class="col-sm-4 text-left ">
                                <input class="form-control " id="" name="address" value="<?php echo $objBankToArray['0']['address']; ?>" required type="text">
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-5"></div>
                        <div class="col-sm-2 text-right form-group">
                            <!-- <button  type="submit" class="btn btn-primary form-control">Submit</button>-->
                            <input type="submit" class="btn-primary form-control" value="Submit">
                        </div>
                        <div class="col-sm-5"></div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </form>
    </div>
    </div>
</div>
