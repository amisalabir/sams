<?php
include_once('../vendor/autoload.php');
include 'namespace.php';


$objBookTitle = new \App\ExpenseIncome\ExpenseIncome();
$objTransaction = new \App\ExpenseIncome\Transaction();
$allClients=$objBookTitle->allClients();

if(isset($_SESSION['mark']))  unset($_SESSION['mark']);
$allData =$objTransaction->setData($_GET);
//$transactionData = $objTransaction->statement();
//var_dump($_GET); die();

if ($singleUser->role=='admin') {

    if ($_GET['bookname'] == 'CASH' && ($_GET['branchid'] == '1' || $_GET['branchid'] == '2' || $_GET['branchid'] == '3') )
        $transactionData = $objTransaction->statement();
    if ($_GET['bookname'] == 'CASH' && $_GET['branchid'] == 'all')
        $transactionData = $objTransaction->bothstatement();
}
else{
    if ($_GET['bookname'] == 'CASH' && ($_GET['branchid'] == '2' || $_GET['branchid'] == '3'))
        $branch="(Yard)";
    $transactionData = $objTransaction->statement();
}
/*Branch selection*/
if($_GET['branchid']=='1') $branch="(Head Office)";
if($_GET['branchid']=='2') $branch="(Yard)";
if($_GET['branchid']=='3') $branch="(Petty Cash-Yard)";
if($_GET['branchid']=='all') $branch="(All Branch)";

//echo "<pre>";var_dump($transactionData); echo "</pre>"; die();
################## statement  block start ####################
/*
if(isset($_REQUEST['fromTransaction']) ) {
    //$someData = $objBookTitle->statement($_REQUEST);
    //$serial = 1;

      echo "<pre>"; var_dump($someData); echo "</pre>"; die();

    $allData =$objTransaction->setData($_GET);
    $allData = $objTransaction->statement();
    //echo "<pre>"; var_dump($allData); echo "</pre>"; die();
/*
    if(($_REQUEST['customerId'])=='all') {
        $allData = $objTransaction->allStatement();
        //echo "<pre>"; var_dump($someData); echo "</pre>"; die();
    }
    if(($_REQUEST['customerId'])!=='all') {
        $allData = $objTransaction->statement();
        $_SESSION['someData'] = $allData;
    }

    $_SESSION['someData']=$allData;
    //echo "<pre>"; var_dump($allData); echo "</pre>"; die();
    Converting Object to an Array
    $objToArray = json_decode(json_encode($allData), True);
    $customeName=$objToArray['0']['name'];
    $statementTotal=$objBookTitle->statementTotal();

}*/
$_SESSION['someData']=$transactionData;
//echo "<pre>"; var_dump($allData); echo "</pre>"; die();
//Converting Object to an Array
$objToArray = json_decode(json_encode($transactionData), True);
//cho "<pre>"; var_dump($objToArray); echo "</pre>"; die();

$customeName=$objToArray['0']['headnameenglish'];


//$statementTotal=$objBookTitle->statementTotal();
$serial = 1;
################## statement  block end ######################


include_once ('header.php');
include_once('printscript.php');
?>
<div align="center" class="content">
    <div align="center" class="container ctn">
        <div align="center" class="container">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success ' id='message'> $msg </div> </div>"; ?>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
        <!-- required for search, block 4 of 5 start -->
        <!-- Search nav start-->
        <?php // include_once ('searchnav.php'); ?>
        <!-- Search nav ended-->
        <!-- Date selection ended -->
        <form action="trashmultiple.php" method="post" id="multiple">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="navbar-header">
                        <button style="background-color: #8aa6c1;" type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#navbarTwo" aria-expanded="false" aria-controls="navbarTwo">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <?php
                    $userButton= "<div id=\"navbarTwo\" class=\"navbar-collapse collapse\">
                        <ul class=\"nav navbar-nav navbar-right\">
                            <button type=\"button\"   id=\"btnPrint\" value=\"Print Div Contents\" class=\"btn btn-primary active \">Print</button>
                            <a href=\"pdf.php\" class=\"btn btn-primary \"  >Download as PDF</a>
                            <a href=\"xl.php\" class=\"btn btn-primary\" \" >Download as XL</a>
                            <a href=\"email.php?list=1\" class=\"btn btn-primary\" role=\"button\" >Email</a>
                            ";
                    $adminButton="<button type=\"button\" class=\"btn btn-danger\" id=\"delete\">Delete  Selected</button>
                            <button type=\"submit\" class=\"btn btn-warning\">Trash Selected</button>
                            <a href=\"trashed . php ? Page = 1\"   class=\"btn btn-info\" > View Trashed List</a>
                            </ul></div>
                        ";
                    if($singleUser->role=='admin'){
                        echo $userButton.$adminButton;
                    } else{echo $userButton;}
                    ?>
                </div>
                <div class="col-md-1"></div>
            </div>
            <!-- <h1 style="text-align: center" ;">Book Title - Active List (<?php //echo count($allData) ?>)</h1>-->
            <span><br><br> </span>
            <div class="container">
                <div id="dvContainer">
                    <style>
                        <?php
                        include ('../resource/css/printsetup.css');


                        ?>
                    </style>
                    <table >
                        <thead>
                        <tr>
                            <td colspan="3" align="center" >
                                <font  style="text-align: center;  text-transform:uppercase; font-weight: bold; font-size:25px;">Bhatiyari Ship Breakers Ltd.</font> <br>
                                <font style="font-size:14px">Bhatiyari, Sitakunda, Chittagong.</font><br>
                                <font style="font-size:13px">(<?php echo "Statement Since : ".$_GET['fromTransaction']." to ".$_GET['toTransaction'];?>)</font>
                            </td>
                        </tr>
                        <tr><td ><b><?php  echo "HEAD: Cash Statement ".$branch; ?></b></td> <td></td> <td style="text-align: right; font-size: 12;"><?php echo "Print Date: ";  echo date('Y-m-d'); ?> </td></tr>
                        </thead>
                        <tr> <td colspan="3">
                                <!-- Inner Table -->
                                <div class="row" align="center">
                                    <div id="reporttable" class="col-sm-12 text-center" align="center" >
                                        <table width="auto"   class="" >
                                            <thead>
                                            <tr style="background-color:#F2F2F2;">
                                                <th class="text-center"><input id="select_all" type="checkbox" value="select all"></th>
                                                <th class="text-center">SL</th>
                                                <th class="text-center">Date</th>
                                                <th class="text-center" width="500px">Description</th>
                                                <th class="text-center">Voucher/ <br>Challan No</th>
                                                <th class="text-center">Received <br> (Taka)</th>
                                                <th class="text-center">Payment <br> (Taka)</th>
                                                <th class="text-center">Balance</th>
                                            </tr>
                                            </thead>
                                            <?php
                                            $totalAmountIn=0;
                                            $totalAmountOut=0;
                                            $balance=0;

                                            foreach($transactionData as $oneData){

                                                if($serial%2) $bgColor = "AZURE";
                                                else $bgColor = "#ffffff";
                                                $totalAmountIn=$totalAmountIn+$oneData->amountIn;
                                                $totalAmountOut=$totalAmountOut+$oneData->amountOut;
                                                //$totalAmount=$totalAmount+$oneData->amount ;
                                                $balance=($balance-$oneData->amountOut)+$oneData->amountIn;
                                                $voucherType=""; $voucherOrChallan="";
                                                if($oneData->voucherNo!=Null){$voucherType="Dr -"; $voucherOrChallan=$oneData->voucherNo;}else{$voucherType="Cr -"; $voucherOrChallan=$oneData->crvoucher;}
                                                if($oneData->challanno!=Null ||($oneData->voucherNo==Null && $oneData->crvoucher==Null )){$voucherType="Ch -"; $voucherOrChallan=$oneData->challanno;}
                                                echo "
                  <tr  style='background-color: $bgColor'>
                     <td style='padding-left:5px;' class='text-center'><input type='checkbox' class='checkbox' name='mark[]' value='$oneData->id'></td>
                     <td style='text-align: center;'>";
                                                if($singleUser->role=='admin'){ echo "<a role='menuitem' tabindex=-1' href='edit.php?id=$oneData->id'>$serial</a>";} else{echo $serial;}
                                                echo"</td>
                      <td class='text-center'>$oneData->transactionDate</td>
                     <td class='text-left'> $oneData->headnameenglish: $oneData->accountname $oneData->product_name $oneData->partyname<br> $oneData->transactionFor $oneData->remarks  </td>
                     <td class='text-center'>$voucherType $voucherOrChallan</td>
                     <td style='text-align: right;'>".number_format($oneData->amountIn,0)."</td>
                     <td style='text-align: right;'>".number_format($oneData->amountOut,0)."</td>
                     <td class='text-right'>".number_format($balance,0)."</td>                     
                     
                  </tr>
              ";
                                                $serial++; }
                                            echo "     
                        <tr style='background-color:; font-weight: bold;'>
                            <td style='text-align: right;' colspan='5'> Total Taka: <span>&nbsp;&nbsp; </span></td>
                            <td class='text-right'>".number_format($totalAmountIn,0)." </td>
                            <td class='text-right'>".number_format($totalAmountOut,0)."</td>
                            <td class='tablefooter'>".number_format($balance,0)." </td>
                        </tr>
                        "; ?>
                                        </table>
                                        <br>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
        <!--  ######################## pagination code block#2 of 2 start ###################################### -->
        <!--  ######################## pagination code block#2 of 2 end ###################################### -->
    </div>
</div>
<?php
include ('footer.php');
include ('footer_script.php');
?>
