<?php
namespace App\ExpenseIncome;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class Head extends  DB{

    private $headnamebangla, $headnameenglish, $position, $relatedform, $accheadId;

    public function setData($postData){

        if(array_key_exists('modifiedDate',$postData)){
            $this->modified_Date = $postData['modifiedDate'];
        }
        if(array_key_exists('headnamebangla',$postData)){
            $this->headnamebangla = $postData['headnamebangla'];
        }
        if(array_key_exists('headnameenglish',$postData)){
            $this->headnameenglish = $postData['headnameenglish'];
        }
         if(array_key_exists('position',$postData)){
            $this->position = $postData['position'];
        }
         if(array_key_exists('relatedform',$postData)){
            $this->relatedform = $postData['relatedform'];
        }
        if(array_key_exists('accheadId',$postData)){
            $this->accheadId = $postData['accheadId'];
        }
    }
    public function store(){

        $arrData = array($this->headnamebangla,$this->headnameenglish,$this->position,$this->relatedform,$this->modified_Date);
        $sql = "INSERT into accounthead(headnamebangla,headnameenglish,position,relatedform,created) VALUES(?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData);
        if($result)
            Message::message("Success! New Head Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('index.php');
    }

    public function viewallhead(){
        //$sql = "select * from salesentry where salesentry.soft_deleted='No'";
        $sql="SELECT * from accounthead where soft_deleted='No' ORDER BY headnameenglish ASC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function viewHead(){

        $sql="SELECT * from accounthead  WHERE id='$this->accheadId'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function delete(){

        $sql = "UPDATE  accounthead SET soft_deleted='Yes' WHERE id=".$this->accheadId;

        $result = $this->DBH->exec($sql);
        if($result)
            Message::message("Success! Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Soft Deleted  :( ");

        Utility::redirect('index.php');
    }

    public function update(){
        //var_dump($_POST); die();
        $arrData = array($this->headnamebangla,$this->headnameenglish);
        $sql = "UPDATE accounthead SET headnamebangla=?,headnameenglish=? WHERE id='$this->accheadId'";
        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData);
        if($result)
            Message::message("Success! New Head Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated :( ");

        Utility::redirect('index.php');
    }


}