<?php
namespace App\ExpenseIncome;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class Party extends  DB{

    private $modifiedDate,$partyname, $address, $phone,$email,$partyid ;

    public function setData($postData){
        if(array_key_exists('modifiedDate',$postData)){
            $this->modifiedDate= $postData['modifiedDate'];
        }
        if(array_key_exists('partyname',$postData)){
            $this->partyname= $postData['partyname'];
        }
        if(array_key_exists('address',$postData)){
            $this->address = $postData['address'];
        }
         if(array_key_exists('phone',$postData)){
            $this->phone = $postData['phone'];
        }
         if(array_key_exists('email',$postData)){
            $this->email = $postData['email'];
        }
        if(array_key_exists('partyid',$postData)){
            $this->partyid = $postData['partyid'];
        }
    }
    public function store(){
        $arrData = array($this->partyname,$this->address,$this->phone,$this->email,$this->modifiedDate);
        $sql = "INSERT into party(partyname,address,phone,email,created) VALUES(?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData);
        if($result)
            Message::message("Success! New Party Has Been Added Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('index.php');
    }
    public function viewallparty(){

        $sql="SELECT * from party where soft_deleted='No' ORDER BY partyname ASC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function viewParty(){

        $sql="SELECT * from party  WHERE id='$this->partyid'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function delete(){

        $sql = "UPDATE  party SET soft_deleted='Yes' WHERE id=".$this->partyid;

        $result = $this->DBH->exec($sql);
        if($result)
            Message::message("Success! Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Soft Deleted  :( ");

        Utility::redirect('index.php');
    }




}